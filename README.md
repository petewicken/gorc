GORC
====

Optimise getting around your system by typing ``go <action>``. For example, quickly get to your networking scripts by running ``go network``, or modify and source your bash config by running ``go bash``. Running ``go gorc`` will take you to the GoRC file where you can add your own quick commands - it's simply a bash switch statement, so just add the ``<action>`` and which commands you want it to run and you're away! ``go list`` by default will show you a list of all your GORC commands, in case you ever forget them.

It's basically fancy bash aliases.


Install
-------

Copy the script to ``~/.gorc.sh``. There's not much point symbolically linking it as you will be adding your own go commands here that you probably don't want to commit back up.

``cp gorc.sh ~/.gorc.sh``

Because you will almost definitely want the script to change your directory, you must add the following line to your shell config (e.g. your .bashrc file).

``source ~/.gorc.sh``

This will now allow you to run ``go <action>``.