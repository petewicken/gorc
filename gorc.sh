# Helpers
function _help() {
    echo "Usage: go <keyword>"
    echo
    echo "Use 'go list' to see available keywords."
}

function _list() {
    grep -e '^\s*"[a-zA-Z0-9\-]*"' ~/.gorc.sh -o | sed 's/\s//g;s/"//g' | sort | column
}

# GoRC
function go() {
    case "$1" in

        # Example Projects
        "git") cd ~/git ;;

        "network") cd /etc/sysconfig/network-scripts ;;
        "systemd") cd /etc/systemd/system ;;
        "cpu") cat /proc/cpuinfo ;;
        "memory") cat /proc/meminfo;;
        "tcp") netstat -natp ;;

        # Dotfiles
        "gorc") vim ~/.gorc.sh && source ~/.gorc.sh;;
        "bash") vim ~/.bashrc && source ~/.bashrc ;;
        "zsh") vim ~/.zshrc && source ~/.zshrc ;;
        "tmux") vim ~/.tmux.conf ;;
        "vim") vim ~/.vimrc ;;

        # Meta
        "") _help ;;
        "help") _help ;;
        "list") _list ;;
        *) echo "'$1' does not have a gorc entry." ;;

    esac
}
